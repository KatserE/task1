enum Sex {
    case Male, Female
}

struct Person : CustomStringConvertible {
    let firstName: String
    let lastName: String
    let age: UInt8
    let sex: Sex

    var description: String {
        """
        \(lastName) \(firstName):
            * Age -- \(age)
            * Sex -- \(sex)
        """
    }
}

let appleCoder = Person(firstName: "Eugene", lastName: "Katser", age: 21, sex: .Male)

print(appleCoder)
