OUTPUT	:= "./output/"
SRC		:= "task1.swift"
NAME	:= "task1"

all:
	@test -d $(OUTPUT) || mkdir $(OUTPUT)
	@swiftc -O $(SRC) -o $(OUTPUT)/$(NAME)
	@echo Compilation is successful!

clean:
	@test -d $(OUTPUT) && rm -rf $(OUTPUT)
